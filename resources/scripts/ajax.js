$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

function ajaxRequest(url, method, data, success_callback, failure_callback) {
	$.ajax({
		url: url,
		type: method,
		data: data
	})
	.done(function(response) {
		success_callback(response);
	})
	.fail(function(response) {
		failure_callback(response);
	});
	// .always(function() {
	// 	console.log("complete");
	// });

}

$(function () {
      $('[data-toggle="tooltip"]').tooltip();
});
