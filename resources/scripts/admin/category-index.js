/*
 * DELETE /admin/category/{id}  -- delete post
 * blade code acesses-
 * GET /admin/category
 */

function newCategory(){
	location.href = '/admin/category/create';
}

// -delete category:
function deleteFailure(data){
	console.log(data);
}

function deleteSuccess(data){
	location.reload();
}

function deleteCategory(category) {
	var id = category.getAttribute('data-categoryId');
	var name = category.getAttribute('data-categoryName');
	var url = '/admin/category/' + id;
	ajaxRequest(url, 'DELETE', {},  deleteSuccess, deleteFailure);
}
// -end delete

// -edit cateogory

function editCategory(category) {
	var id = category.getAttribute('data-categoryId');
	location.href = '/admin/category/' + id + '/edit';
}

function viewDataCategory(category) {
	var id = category.getAttribute('data-categoryId');
	location.href = '/admin/post?category=' + id;
}
