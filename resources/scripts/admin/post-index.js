/*
 * DELETE /admin/category/{id}  -- delete post
 * blade code acesses-
 * GET /admin/category
 */

function postFailure(data){
	console.log(data);
}

function postSuccess(data){
	location.reload();
}

function newPost(){
	var $inputs = $('#create-post :input');
	var values = {};
    values.category = $('#categoryId').attr('data-categoryId');
	$inputs.each(function() {
		values[this.name] = $(this).val();
	});

	var url = '/admin/post';
	ajaxRequest(url, 'POST', values, postSuccess, function(data){
	    var response = JSON.parse(data.responseText);

	    var html = '<ul>';
	    for(var error in response) {
		    html += "<li> " + response[error] + "</li>";
	    }
	    html += "</ul>";
	    $('#create-error').html(html);
	    $('#create-error').show();
    });

	return false;
}


// delete an entire post
function deletePost(post) {
	var id = post.getAttribute('data-postId');
	var name = post.getAttribute('data-postName');
	var url = '/admin/post/' + id;
	ajaxRequest(url, 'DELETE', {},  postSuccess, postFailure);
}



function editFailure(data){
	console.log(data);
}

function editSuccess(data){
	$('#post-content').html(data);
}

// retrive edit form for post
function editPost(post) {
	var id = post.getAttribute('data-postId');
	var url = '/admin/post/' + id + '/edit';
	ajaxRequest(url, 'GET', {},  editSuccess, editFailure);
}

function createPost() {
    var url = '/admin/post/create';
    ajaxRequest(url, 'GET', {}, editSuccess, editFailure);
}

function responseFailure(data){
	console.log(data);
}

function responseSuccess(data){
	var url = '/admin/post/' + data.id + '/edit';
	// code reuse
	ajaxRequest(url, 'GET', {},  editSuccess, editFailure);
}

// append post postelements
function appendPostElements(){
	var $inputs = $('#create-postelement :input');
	var $postId = $('#create-postelement').attr('data-postId');
	var values = {};
	$inputs.each(function() {
		values[this.name] = $(this).val();
	});
	var url = '/admin/post/' + $postId + '/append-elements';
	ajaxRequest(url, 'POST', values,  responseSuccess, responseFailure);

	return false;
}

function requestPostElements(formName, callback){
	var inputs = $('#' + formName +' :input');
	var postId = $('#' + formName).attr('data-postId');
	var values = {};
    var list = [];

    /*
     * Checkbox is checkbox_postid
     * Make a list of checked post
     */
	inputs.each(function() {
        var element = this.name.split('_');
        if (element[0] == 'checkbox' && $(this).is(':checked')) {
            list.push(element[1]);
        }

        // csfr token
        if (element[1] == 'token') {
            values._token = $(this).val();
        }
    });

    /*
     * Value is value_postid
     * Use list of checked post to determine what value to update.
     */

	inputs.each(function() {
        var element = this.name.split('_');
        if (element[0] == 'value' && $.inArray(element[1], list) != -1) {
		    values[element[1]] = $(this).val();
        }
	});

    callback(postId, values);
}


// update post elements
function updatePostElements(){
    requestPostElements('update-postelements', function(postId, values) {
	    var url = '/admin/post/' + postId + '/update-elements';
	    ajaxRequest(url, 'PUT', values,  responseSuccess, responseFailure);
    });

	return false;
}

// delete post elements
function deletePostElements(){
    requestPostElements('update-postelements', function(postId, values) {
	    var url = '/admin/post/' + postId + '/delete-elements';
	    ajaxRequest(url, 'DELETE', values,  responseSuccess, responseFailure);
    });

	return false;
}

$(function(){
    createPost();
});
