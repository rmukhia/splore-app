var FieldForm = React.createClass({
  render : function() {
    return (
      <fieldset className="form-group row">
        <div className="col-xs-5 col-md-5">
        <input onChange={this.handleNameChange} type="text" name="name" className="form-control" placeholder="New field"/>
        </div>
        <div className="col-xs-3 col-md-3">
        <select onChange={this.handleTypeChange} className="form-control">
            <option value="string">String</option>
            <option value="number">Number</option>
        </select>
        </div>
        <div className="col-xs-3 col-md-3">
        <select onChange={this.handleMultiChange}  className="form-control">
            <option value="false">Single value</option>
            <option value="true">Multi value</option>
        </select>
        </div>
        <div className="col-xs-1 col-md-1">
          <button onClick={this.handleNewField}className="btn btn-info pull-right">
            <span className='glyphicon glyphicon-plus'></span>
          </button>
        </div>
      </fieldset>
    )
  },
  getInitialState: function(){
    return {name: '', type: 'string', multi: "false"};
  },
  handleNameChange: function(e){
    this.setState({name: e.target.value});
  },
  handleTypeChange: function(e){
    this.setState({type: e.target.value});
  },
  handleMultiChange: function(e){
    this.setState({multi: e.target.value});
  },
  handleNewField: function(e){
    var field = {
      name: this.state.name,
      type: this.state.type,
      multi: this.state.multi == "true" ? true: false
    }
    this.props.handleNewField(field);
  }
});

var Field = React.createClass({
  render : function() {
    return(
      <tr>
        <td>{this.props.element.name}</td>
        <td>{this.props.element.type}</td>
        <td>{this.props.element.multi ? 'Multiple value': 'Single value'}</td>
        <td><div className="dropdown pull-right">
          <button className="btn btn-default dropdown-toggle" data-toggle="dropdown">
              <span className="glyphicon glyphicon-chevron-down"></span>
          </button>
          <ul className="dropdown-menu">
            <li><a href="javascript:void(0)" onClick={this.handleMoveUp}>
              Move Up
              </a>
            </li>
            <li><a href="javascript:void(0)" onClick={this.handleMoveDown}>
              Move Down
              </a>
            </li>
            <li>
              <a href="javascript:void(0)" onClick={this.handleFieldMoveDown}>
                Delete
              </a>
            </li>
          </ul>
        </div></td>
    </tr>
    )
  },
  getInitialState: function(){
    return {name: ''}
  },
  componentDidMount: function(){
    this.setState({name: this.props.element.name});
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({name: nextProps.element.name});
  },
  handleMoveUp : function(e){
    this.props.handleMoveUp(this.state.name);
  },
  handleMoveDown : function(e){
    this.props.handleMoveDown(this.state.name);
  },
  handleFieldMoveDown : function(e){
    this.props.handleFieldMoveDown(this.state.name);
  }
})

var FieldList = React.createClass({
  render : function() {
    var self = this;
    var elementList = this.props.schema.map(function(element){
      return (
        <Field key={element.name} element={element} handleMoveUp={self.props.handleMoveUp}
          handleMoveDown={self.props.handleMoveDown}
          handleFieldMoveDown={self.props.handleFieldMoveDown}/>
      )
    });
    return (
      <table className="table table-condensed table-hover">
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Multiple</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
          {elementList}
        </tbody>
      </table>
    )
  }
})

var FieldEditor = React.createClass({
  render : function() {
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <div className="row">
            <div className="col-xs-10 col-md-10">
              <h3 className="panel-title">Fields</h3>
            </div>
            <div className="col-xs-2 col-md-2">
              <div className="pull-right">
              <button className="btn btn-success dropdown-toggle" data-toggle="dropdown">
                <span className="glyphicon glyphicon-chevron-down"></span>
              </button>
              <ul className="dropdown-menu">
                <li><a href="javascript:void(0)" onClick={this.props.handleTemplateDownload}>
                  Download Template
                  </a>
                </li>
              </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="panel-body">
          <FieldList schema={this.props.schema} handleMoveUp={this.props.handleMoveUp}
            handleMoveDown={this.props.handleMoveDown} handleFieldMoveDown={this.props.handleFieldMoveDown}/>
        </div>
        <div className="panel-footer">
          <FieldForm handleNewField={this.props.handleNewField}/>
        </div>
      </div>)
  }
});

var CategoryDetails = React.createClass({
  render: function(){
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <h5 className="panel-title">Category Details</h5>
        </div>
        <div className="panel-body">
          <div className="form-group">
            <h4>Name</h4>
            <input type="text" className="form-control" onChange={this.handleNameChange} value={this.state.name || ''}/>
          </div>
          <div className="form-group">
            <h4>Id</h4>
            <input type="text" className="form-control" disabled onChange={this.yada} value={this.props.details.id || ''}/>
          </div>
          <div className="form-group">
            <h4>Created At</h4>
            <input type="text" className="form-control" disabled onChange={this.yada} value={this.props.details.createdAt || ''}/>
          </div>
          <div className="form-group">
            <h4>Updated At</h4>
            <input type="text" className="form-control" disabled onChange={this.yada} value={this.props.details.updatedAt || ''}/>
          </div>
          <div className="form-group">
            <button onClick={this.props.handleCategoryUpdate} className="btn btn-primary pull-right">Update</button>
          </div>
        </div>
      </div>
    )
  },
  getInitialState : function () {
    return {name: undefined};
  },
  componentDidMount: function () {
    this.setState({name : this.props.details.name});
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({name : nextProps.details.name});
  },
  handleNameChange: function(e) {
    this.setState({name : e.target.value});
    this.props.handleNameChange(e.target.value);
  },
  yada :function(e){

  }
});

var APIDetails = React.createClass({
  render: function(){
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <h5 className="panel-title">API Details</h5>
        </div>
        <div className="panel-body">
          <pre>api/index/meta</pre>
          <p> List of categories. </p>
          <pre>api/meta/name/{this.props.details.name}</pre>
          <p> Category details and schema for {this.props.details.name}.</p>
          <pre>api/index/{this.props.details.name}/[page]?num=[num]</pre>
          <p> Posts list for {this.props.details.name}.
          This list is paginated. [page] is the page number of the list to retrieve. [num] is the number of Posts per page. Default is 10.
          </p>
          <pre>api/{this.props.details.name}/[postId]</pre>
          <p> Get a single post with [postId] from category {this.props.details.name}</p>
        </div>
      </div>
    )
  }
});


var CategoryEditor = React.createClass({
  render: function(){
    return (
      <div className="row">
        <div className="col-xs-12 col-md-3">
          <CategoryDetails details={this.state.details} handleNameChange={this.handleNameChange} handleCategoryUpdate={this.handleCategoryUpdate}/>
        </div>
        <div className="col-xs-12 col-md-9">
          <FieldEditor schema={this.state.schema} handleMoveUp={this.handleMoveUp}
               handleMoveDown={this.handleMoveDown} handleFieldMoveDown={this.handleFieldMoveDown}
               handleNewField={this.handleNewField} handleTemplateDownload={this.handleTemplateDownload}/>
          <APIDetails details={this.state.details}/>
        </div>
      </div>
      )
  },
  getInitialState: function(){
    return {schema: [], details: {}};
  },
  componentDidMount: function(){
    var self = this;
    var categoryId = $('#category-editor').attr('data-categoryId');
    var url = '/api/meta/' + categoryId;
    $.getJSON(url, function(meta){
      var schema = JSON.parse(meta.schema);
      var details = {
          id : meta.id,
          name : meta.name,
          createdAt : meta.created_at,
          updatedAt : meta.updated_at
      };
      var result = {};

      for (var key in schema.type) {
        result[key] = {
          type : schema.type[key],
          multi : ''
        }
      }

      for (var key in schema.multiple) {
        result[key].multi = schema.multiple[key]
      }

      schema = [];
      var i =0;
      for (var key in result) {
        var temp = {
          name : key,
          type : result[key].type,
          multi : result[key].multi
        }
        schema.push(temp);
      }
      self.setState({schema: schema, details: details});
    });
  },
  handleNewField(field){
    var schema = this.state.schema;
    // if name is "" do not process
    if (field.name == "") {
      return;
    }

    // if name is already present in schema, do not process
    for (var id in schema) {
      if (schema[id].name == field.name) {
          return;
      }
    }
    schema.push(field);
    this.setState({schema: schema});
    this.handleCategoryUpdate();
  },
  handleMoveUp : function(name){
    var schema = this.state.schema;
    for(var id in schema) {
      if (schema[id].name == name) {
        var field = schema[id];
        if (id == 0) {
          continue;
        }
        schema.splice(id - 1, 0, schema.splice(id, 1)[0]);
      }
    }
    this.setState({schema: schema});
    this.handleCategoryUpdate();
  },
  handleMoveDown : function(name){
    var schema = this.state.schema;
    for(var id in schema) {
      if (schema[id].name == name) {
        var field = schema[id];
        if (id == schema.length - 1) {
          continue;
        }
        schema.splice(id +1 , 0, schema.splice(id, 1)[0]);
      }
    }
    this.setState({schema: schema});
    this.handleCategoryUpdate();
  },
  handleFieldMoveDown : function(name){
    var schema = this.state.schema;
    for(var id in schema) {
      if (schema[id].name == name) {
        schema.splice(id, 1);
      }
    }
    this.setState({schema: schema});
    this.handleCategoryUpdate();
  },
  handleNameChange: function(name){
    var details = this.state.details;
    details.name = name;
    this.setState({details: details});
  },
  handleCategoryUpdate : function(){
    var name = this.state.details.name;
    var schema = this.state.schema;
    var type = {};
    var multiple = {};
    var request = {
      name : name,
      schema: ""
    };

    for(var id in schema) {
      var field = schema[id];
      type[field.name] = field.type;
      multiple[field.name] = field.multi;
    }

    request.schema = JSON.stringify(
            {
              type : type,
              multiple : multiple
            });
    ajaxRequest('/admin/category/' + this.state.details.id , 'PATCH', request,
      function(){
          //success
      },
      function() {
         //failure
      });
  },
  handleTemplateDownload : function(){
    var url = '/admin/spreadsheet/' + this.state.details.id;
    location.href = url;
  }
});


ReactDOM.render(
  <CategoryEditor/>,
  document.getElementById('category-editor')
);
