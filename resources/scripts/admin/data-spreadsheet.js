/*
 * POST /admin/data/spreadsheet  -- Upload Post
 */


/* When submit fails with some validation error*/
function createFailure(data) {
	var response = JSON.parse(data.responseText);

	var html = '<ul>';
	for(var error in response) {
		html += "<li> " + response[error] + "</li>";
	}
	html += "</ul>";
	$('#create-error').html(html);
	$('#create-error').show();
}

function createSuccess(data) {
	//location.href = "/admin/category";
  $('#create-error').hide();
}

/* When Submit button is pressed */
$('#spreadsheet-form1').submit(function(event) {
	/* Act on the event */


	return true;
});
