function deleteUser(user){
	var id = user.getAttribute('data-userId');
	var url = '/admin/user/' + id;
	ajaxRequest(url, 'DELETE', {},
        function(data){
	        location.reload();
        }, 
        function(data) {
            console.log(data);
        });
}

function promoteUser(user){
	var id = user.getAttribute('data-userId');
	var url = '/admin/user/' + id + '/promote';
	ajaxRequest(url, 'PUT', {},
        function(data){
	        location.reload();
        }, 
        function(data) {
            console.log(data);
        });
}

function demoteUser(user){
	var id = user.getAttribute('data-userId');
	var url = '/admin/user/' + id + '/demote';
	ajaxRequest(url, 'PUT', {},
        function(data){
	        location.reload();
        }, 
        function(data) {
            console.log(data);
        });
}
