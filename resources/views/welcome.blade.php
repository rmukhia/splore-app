@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <a href="https://gitlab.com/rmukhia/splore-app" target="_blank">How to use this app.</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
