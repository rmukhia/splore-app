
<legend>{{ $post->name }}</legend>
@if (count($postElements) > 0)
<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title">Edit</h3>
</div>
<div class="panel-body">
  <form id="update-postelements" data-postId="{{ $post->id }}">
    {{ csrf_field() }}

    @foreach ($postElements as $postElement)
    <div class="row form-group">
      <div class="col-sm-1 col-md-1 col-lg-1">
        <input  type="checkbox" name="checkbox_{{ $postElement['id'] }}"></input>
      </div>
      <div class="col-sm-2 col-md-2 col-lg-2">
        <p>{{ $postElement['name'] }}</p>
      </div>
      <div class="col-sm-9 col-md-9 col-lg-9">
        <input name="value_{{ $postElement['id'] }}" type="{{ $postElement['htmlType'] }}" class="form-control" value="{{ $postElement['content']}}" name={{ $postElement['name'] }}/>
      </div>
    </div>
    <!-- TODO: date -->
    @endforeach
    <div class="row form-group">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="pull-right">
          <input type="button" name="update" class="btn btn-primary" value="Update Selected" onclick="updatePostElements()"/>
          <input type="button" name="delete" class="btn btn-danger" value="Delete Selected" onclick="deletePostElements()"/>
        </div>
    </div>
  </form>
  </div>
</div>
</div>

<div>
&nbsp;
</div>
@endif

@if (count($addFields) > 0)
<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title">Add</h3>
</div>
<div class="panel-body">
  <form id="create-postelement" data-postId="{{ $post->id }}">
    {{ csrf_field() }}
    @foreach ($addFields as $field)
    <div class="row form-group">
      <div class="col-sm-3 col-md-3 col-lg-3">
        <p> {{ $field['name'] }} </p>
      </div>
      <div class="sol-cm-9 col-md-9 col-lg-9">
        <input type="{{ $field['htmlType'] }}" class="form-control" name="{{ $field['name'] }}"/>
      </div>
    </div>
  @endforeach
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="pull-right">
        <input type="button" name="add" class="btn btn-primary" value="Add" onclick="appendPostElements()"/>
      </div>
    </div>
  </div>
  </form>
</div>
</div>
@endif
