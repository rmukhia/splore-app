@extends('admin.master')

@section('breadcrumbs')
<li><a href="{{ url('admin/category')}}"/>Category</a></li>
<li class="active">View Data</li>
@endsection


@section('admincontent')
  <div id="categoryId" data-categoryId="{{ $categoryId }}"></div>
  <div class="row">
  <div class="col-sm-3 col-md-3 col-lg-3">
    <table class="table table-condensed table-hover panel panel-default">
  		<thead class="panel-heading">
  			<tr>
  				<th><h3 class="panel-title">Name</h3></th>
  				<th>
  					<button type="button" class="btn btn-success pull-right" onclick="createPost()" data-toggle="tooltip" title="Create a new post.">
  						New
  					</button>
  				</th>
  			</tr>
  		</thead>
  		@if (count($posts) > 0)
  		<tbody class="panel-body">
  			@foreach ($posts as $post)
  				<tr>
  					<td class="hand-cursor" onclick="editPost(this)"
            data-postId="{{ $post->id }}"
            data-postName="{{ $post->name }}">
            {{ $post->name }}</td>
  					<td>
  						<div class="dropdown pull-right">
   							<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-chevron-down"></span>
                            </button>
  		 					<ul class="dropdown-menu">
  		 						<li><a href="javascript:void(0)" onclick="editPost(this)"
  		 							data-postId="{{ $post->id }}"
  		 							data-postName="{{ $post->name }}">
  		 							Edit
  		 							</a>
  		 						</li>
  		 						<li>
  		 							<a href="javascript:void(0)" onclick="deletePost(this)"
  		 							data-postId="{{ $post->id }}"
  		 							data-postName="{{ $post->name }}">
  		 							Delete
  		 							</a>
  		 						</li>
  		 					</ul>
   						</div>
  					</td>
  				</tr>
  			@endforeach
  		</tbody>
  		@endif
  	</table>
  </div>
  <div class="col-sm-9 col-md-9 col-lg-9" id="post-content">
  </div>
</div>
@endsection

@section('scripts')
<script src="/js/admin/post-index.js" type="text/javascript"></script>
@endsection
