@extends('admin.master')

@section('css')
<link rel="stylesheet" type="text/css" href="/css/admin/category-create.css">
@endsection

@section('breadcrumbs')
<li><a href="{{ url('admin/category')}}">Category</a></li>
<li class="active">Create</li>
@endsection

@section('admincontent')
<div class="panel panel-default">
<div class="panel-heading">
	<h3 class="panel-title">New Category</h3>
</div>
<div class="panel-body">
<form action="/admin/category" method="POST" role="form" id="create-form">
	{{ csrf_field() }}
	<div class="form-group">
		<label>Name</label>
		<input type="text" class="form-control" name="name">
	</div>
    @if(count($errors) > 0) 
	<div>&nbsp;</div>
    <div class="alert alert-danger" id="create-error">
        <ul>
        @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
        </ul>
    </div>
    @endif
	<div class="pull-right">
		<button type="submit" class="btn btn-primary">Create</button>
	</div>
</form>
</div>
</div>
@endsection

@section('scripts')
@endsection
