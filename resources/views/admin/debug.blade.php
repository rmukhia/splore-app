@extends('admin.master')

@section('breadcrumbs')
<li class="active">Debug</li>
@endsection

@section('admincontent')
<div class="panel panel-default">
<div class="panel-heading">
	<h3 class="panel-title">Debug</h3>
</div>
<div class="panel-body">
<div class="row">
    <div class="col-xs-2 col-md-2">
    <!-- Button one -->
        <form action={{ url('/admin/debug/post') }} method="POST">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <input type="submit" class="btn btn-danger" value="Purge All Posts"/>
        </form>
    </div>
    <div class="col-xs-2 col-md-2">
    <!-- Button two -->
    </div>
    <div class="col-xs-2 col-md-2">
    <!-- Button three -->
    </div>
    <div class="col-xs-2 col-md-2">
    <!-- Button four -->
    </div>
    <div class="col-xs-2 col-md-2">
    <!-- Button five -->
    </div>
    <div class="col-xs-2 col-md-2">
    <!-- Button six -->
    </div>
</div>
</div>
</div>

@endsection

@section('scripts')
@endsection
