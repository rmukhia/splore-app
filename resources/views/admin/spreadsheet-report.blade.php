@extends('admin.master')

@section('breadcrumbs')
<li class="active">Spreadsheet Report</li>
@endsection

@section('admincontent')
<div class="panel panel-default">
<div class="panel-heading">
	<h3 class="panel-title">Report</h3>
</div>
<div class="panel-body">
<ul class="nav nav-tabs">
  <li role="presentation"><a href="javascript:void(0)">Report</a></li>
  <li role="presentation" class="active"><a href="javascript:void(0)">JSON</a></li>
</ul>

<pre id="json-stats">
{{ json_encode($stats, JSON_PRETTY_PRINT) }}
</pre>
</div>
</div>

@endsection

@section('scripts')
@endsection
