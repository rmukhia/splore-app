
@extends('admin.master')

@section('breadcrumbs')
<li class="active">Users</li>
@endsection


@section('admincontent')
	<table class="table table-condensed table-hover panel panel-default">
		<thead class="panel-heading">
			<tr>
				<th><h3 class="panel-title">Role</h3></th>
				<th><h3 class="panel-title">Name</h3></th>
				<th>
				</th>
			</tr>
		</thead>
		@if (count($users) > 0)
		<tbody class="panel-body">
			@foreach ($users as $user)
				<tr>
                    <td>
                    @if($user->isAdmin())
                        Admin
                    @endif
                    </td>
					<td>{{ $user->name }}</td>
					<td>
						<div class="dropdown pull-right">
 							<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-chevron-down"></span>
                            </button>
		 					<ul class="dropdown-menu">
		 						<li>
		 							<a href="#" onclick="deleteUser(this)"
		 							data-userId="{{ $user->id }}" >
		 							Delete
		 							</a>
		 						</li>
		 						<li>
                                @if(!$user->isAdmin())
		 							<a href="#" onclick="promoteUser(this)"
		 							data-userId="{{ $user->id }}" >
		 							Promote To Admin
		 							</a>
                                @else
		 							<a href="#" onclick="demoteUser(this)"
		 							data-userId="{{ $user->id }}" >
		 							Demote Admin
		 							</a>
                                @endif
		 						</li>
		 					</ul>
 						</div>
					</td>
				</tr>
			@endforeach
		</tbody>
		@endif
	</table>
</div>

@endsection

@section('scripts')
<script src="/js/admin/user-index.js" type="text/javascript"></script>
@endsection
