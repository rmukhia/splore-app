@extends('admin.master')

@section('breadcrumbs')
<li class="active">Category</li>
@endsection

@section('admincontent')

<table class="table table-condensed table-hover panel panel-default">
  <thead class="panel-heading">
    <tr>
      <th><h3 class="panel-title">Posts</h3></th>
      <th><h3 class="panel-title">Name</h3></th>
      <th>
        <button type="button" class="btn btn-success pull-right" onclick="newCategory()" data-toggle="tooltip" title="Create a new category.">
        New
        </button>
      </th>
    </tr>
  </thead>
  @if (count($categories) > 0)
  <tbody class="panel-body">
  @foreach ($categories as $category)
    <tr >
      <td class="hand-cursor" onclick="viewDataCategory(this)"
        data-categoryId="{{ $category->id }}"
        data-categoryName="{{ $category->name }}"
      >{{ $category->numPost }}</td>
      <td class="hand-cursor" onclick="viewDataCategory(this)"
        data-categoryId="{{ $category->id }}"
        data-categoryName="{{ $category->name }}"
      >{{ $category->name }}</td>
      <td><div class="dropdown pull-right">
        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-chevron-down"></span>
        </button>
        <ul class="dropdown-menu">
          <li><a href="javascript:void(0)" onclick="editCategory(this)"
            data-categoryId="{{ $category->id }}"
            data-categoryName="{{ $category->name }}">
            Edit
            </a>
          </li>
          <li><a href="javascript:void(0)" onclick="viewDataCategory(this)"
            data-categoryId="{{ $category->id }}"
            data-categoryName="{{ $category->name }}">
            View Data
            </a>
          </li>
          <li>
            <a href="javascript:void(0)" onclick="deleteCategory(this)"
              data-categoryId="{{ $category->id }}"
              data-categoryName="{{ $category->name }}">
              Delete
            </a>
          </li>
        </ul>
      </div></td>
    </tr>
  @endforeach
  </tbody>
@endif
</table>

@endsection

@section('scripts')
<script src="/js/admin/category-index.js" type="text/javascript"></script>
@endsection
