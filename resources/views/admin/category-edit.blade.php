
@extends('admin.master')

@section('css')
<link rel="stylesheet" type="text/css" href="/css/admin/category-create.css">
<style type="text/css">
    .hidden { display:none; }
</style>
@endsection

@section('breadcrumbs')
<li><a href="{{ url('admin/category')}}"/>Category</a></li>
<li class="active">Edit</li>
@endsection

@section('admincontent')
<div id="category-editor" data-categoryId="{{ $category->id }}"></div>
@endsection

@section('scripts')
<!-- React -->
<script src="/js/vendor/react/react.js" type="text/javascript"></script>
<script src="/js/vendor/react/react-dom.js" type="text/javascript"></script>
<script src="/js/vendor/babel/browser.min.js"></script>
<script src="/js/admin/components/category-edit.jsx" type="text/babel"></script>
@endsection
