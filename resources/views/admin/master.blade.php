@extends('layouts.app')
@section('content')
<ol class="breadcrumb breadcrumb-shadow">
  <li><a href="{{ url('admin')}}"/>Admin</a></li>
  @yield('breadcrumbs')
</ol>
@yield('admincontent')
@endsection
