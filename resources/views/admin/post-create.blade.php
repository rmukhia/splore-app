<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title">Add</h3>
</div>
<div class="panel-body">
  <form id="create-post">
    {{ csrf_field() }}

    <div class="row">
      <div class="col-sm-2 col-md-2 col-lg-2">
        <p>Name</p>
      </div>
      <div class="coll-sm-10 col-md-10 col-lg-10">
        <input name="name" class="form-control" }}/>
      </div>
    </div>
    <div>
        &nbsp;
    </div>
    <div class="alert alert-danger" id="create-error" hidden="true">
    </div>
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="pull-right">
          <input type="button" name="delete" value="Create" class="btn btn-primary" onclick="newPost()"/>
        </div>
      </div>
    </div>
  </form>
  </div>
</div>
</div>
