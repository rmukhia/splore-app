@extends('admin.master')

@section('css')
@endsection

@section('breadcrumbs')
<li class="active">Upload Data Spreadsheet</li>
@endsection

@section('admincontent')
<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title">Upload Data Spreadsheet</h3>
</div>
<div class="panel-body">
<form  action="/admin/spreadsheet" method="POST" role="form" id="spreadsheet-form" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="form-group">
		<label>File</label>
		<input type="file" class="form-control" name="spreadsheet-file">
	</div>
	<div>&nbsp;</div>
    <div class="alert alert-danger" id="create-error" hidden="true">
    </div>
	<div class="pull-right">
		<button type="submit" class="btn btn-primary">Upload</button>
	</div>
</form>
</div>
</div>
@endsection

@section('scripts')
<script src="/js/admin/data-spreadsheet.js" type="text/javascript"></script>
@endsection
