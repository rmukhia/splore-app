@extends('admin.master')

@section('breadcrumbs')
<li class="active">Dashboard</li>
@endsection


@section('admincontent')
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-xs-10 col-md-10">
        <h3 class="panel-title">Users</h3>
      </div>
      <div class="col-xs-2 col-md-2">
        <button class="btn btn-success pull-right" onclick="location.href='/admin/user';" data-toggle="tooltip" title="Tinker with users.">
            <span class="glyphicon glyphicon-wrench"></span>
        </button>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-xs-6 col-md-4">
        <h4>Total</h4>
        <ul class="list-group">
          <li class="list-group-item"><span class="badge">{{ $users->count }}</span>Count</li>
        </ul>
      </div>
      <div class="col-xs-6 col-md-4">
        <h4>Administrators</h4>
        <ul class="list-group">
          <li class="list-group-item"><span class="badge">{{ $users->adminCount }}</span>Count</li>
        </ul>
      </div>
      <div class="col-xs-6 col-md-4">
        <h4>Regular Users</h4>
        <ul class="list-group">
          <li class="list-group-item"><span class="badge">{{ $users->regularCount }}</span>Count</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <div class="row">
      <div class="col-xs-10 col-md-10">
        <h3 class="panel-title">Data</h3>
      </div>
      <div class="col-xs-2 col-md-2">
        <div class="pull-right">
        <button class="btn btn-success" onclick="location.href='/admin/spreadsheet';" data-toggle="tooltip" title="Upload spreadsheet to populate application data.">
            <span class="glyphicon glyphicon-upload"></span>
        </button>
        &nbsp;
        <button class="btn btn-success"onclick="location.href='/admin/category';" data-toggle="tooltip" title="Tinker with application data.">
            <span class="glyphicon glyphicon-wrench"></span>
        </button>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-body">
    <div class="row">
    @foreach($categories as $category)
      <div class="col-xs-6 col-md-4">
        <h4>{{ $category->name }}</h4>
        <ul class="list-group">
          <li class="list-group-item"><span class="badge">{{ $category->posts->count() }}</span>Posts</li>
        </ul>
      </div>
    @endforeach
    </div>
  </div>
</div>
@endsection

@section('scripts')
@endsection
