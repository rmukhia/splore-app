<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postelements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_fk')->unsigned();
            $table->foreign('post_fk')->references('id')->on('posts');
            $table->string('type');
            $table->string('name');
            $table->decimal('numeric_content')->nullable();
            $table->longText('text_content')->nullable();
            $table->timestamp('date_content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('postelements');
    }
}
