var elixir = require('laravel-elixir');

/*
 *Jade Support
 * Who wants to write messy HTML anyways
 */
//require('laravel-elixir-jade');

/*
 * JS is a messy language.
 * JSHint please show me where the dirt is.
 */
 require( 'elixir-jshint' );


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

	// compile sass
    mix.sass('app.scss');
    mix.sass('admin/category-create.scss',
        'public/css/admin/category-create.css');


    // compile jade
    // mix.jade({
    //     search: '**/*.jade'
    //     });

	  // run jshint on .js files
    mix.jshint( ['resources/scripts/**/*.js'] );
    mix.copy('resources/scripts/', 'public/js');
    mix.copy('bower_components/jquery/dist/jquery.js',
    	'public/js/vendor/jquery.js');

    mix.copy('bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
    	'public/js/vendor/bootstrap.js');
    mix.copy('bower_components/bootstrap-sass/assets/fonts/bootstrap/',
        'public/fonts/bootstrap/');

    //mix.copy('bower_components/ace-builds/src-min-noconflict',
    //   'public/js/vendor/ace-builds');

    mix.copy('bower_components/react/',
        'public/js/vendor/react');

    mix.copy('bower_components/babel/',
        'public/js/vendor/babel');
});
