gulp
tar -zcvf app.tgz app artisan bootstrap bower.json composer.json composer.lock config database package.json public readme.md resources server.php tests storage
scp app.tgz $1:~/tmp/
ssh $1 'mv ~/public_html/.env ~/tmp/.env; cd ~/public_html; rm * -rf; tar -zxvf ~/tmp/app.tgz; php ~/composer.phar install; mv ~/tmp/.env .env'
rm app.tgz
