<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\TokenReplacer\TokenReplacer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register the token replacement service
        $this->app->singleton('App\Helpers\TokenReplacer\Contracts\TokenReplacerContract',
        function($app)
        {
            return new TokenReplacer();
        });
    }
}
