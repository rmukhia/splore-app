<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostElement extends Model
{
    //
    protected $table = 'postelements';

    public function category()
    {
      return $this->belongsTo('App\Post', 'post_fk');
    }
}
