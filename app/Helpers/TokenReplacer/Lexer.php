<?php
namespace App\Helpers\TokenReplacer;

class Lexer {
    private $categoryFormat;
    private $tokenRegex = [];
    // A single regex which will match all pattern.
    private $totalTokenRegex = ''; 

    private $tokens = [];

    // Regex helper functions
    private function orRegex($regex1, $regex2) {
        return $regex1 . '|' . $regex2;
    }

    private function delimRegex($regex) {
        return '/' . $regex . '/';
    }

    private function limitRegex($regex) {
        return '^' . $regex . '$';
    }

    // Regex initialization
    private function initRegex() {
        $specialCharacters = '_@\-';

        $singleTokenRegex = '\#(?P<namesingle>[' . $specialCharacters 
            . '[:alnum:]]+)\#';
        $this->tokenRegex[TokenType::SINGLE] = $singleTokenRegex;

        $multiTokenRegex = '\#(?P<namemulti>[' . $specialCharacters 
            . '[:alnum:]]+)\[(?P<start>[-[:digit:]]*)(?P<continue>:?)(?P<end>[-[:digit:]]*)\]\#';
        $this->tokenRegex[TokenType::MULTIPLE] = $multiTokenRegex;

        $this->totalTokenRegex = $this->orRegex($singleTokenRegex, $multiTokenRegex);  
    }

    // Constuctor
    public function __construct($categoryFormat) {
        $this->categoryFormat = $categoryFormat;
        $this->initRegex();
    }

    // Compile
    public function compile(){
        $offset = 0;
        $len = strlen($this->categoryFormat);
        $this->tokens = [];

        while ($offset <= $len) {
            if (preg_match($this->delimRegex($this->totalTokenRegex), 
            $this->categoryFormat, $matches, PREG_OFFSET_CAPTURE, $offset)) {
                $match_string = $matches[0][0];
                $match_len = strlen($match_string);
                $match_offset = $matches[0][1];
                
                $fillerToken = $this->addFillers($offset, $match_offset, $this->categoryFormat);
                array_push($this->tokens, $fillerToken);

                $token = $this->addToken($match_offset, $match_len, $this->categoryFormat);
                array_push($this->tokens, $token);
                $offset = $match_offset + $match_len;
            }
            else {
                $fillerToken = $this->addFillers($offset, $len, $this->categoryFormat);
                array_push($this->tokens, $fillerToken);
                break;
            }

        }
    }

    // Lexer functions
    private function addFillers($offset, $match_offset, $string) {
        $token = new Token;
        $token->type = TokenType::FILLER;
        $token->value = substr($string, $offset, $match_offset - $offset);
        return $token;
    }

    private function addToken($match_offset, $match_len, $string) {
        $token = new Token;
        $subject = substr($string, $match_offset, $match_len);
        $index = '';

        foreach($this->tokenRegex as $type => $regex) {
            $currRegex = $this->delimRegex($this->limitRegex($regex));
            if (preg_match($currRegex, $subject, $matches)) {
                $token->type = $type;
                $token->value = $subject;
                switch ($type) {
                case TokenType::SINGLE:
                    $token->id = $matches['namesingle'];
                    break;
                case TokenType::MULTIPLE:
                    $token->id = $matches['namemulti'];
                    $token->startIndex = intval($matches['start']);
                    $token->endIndex = intval($matches['end']);
                    $token->continueToEnd = false;
                    $token->continueFromStart = false;

                    if ($matches['continue'] != '') {
                        if ($token->startIndex == 0) {
                            $token->continueFromStart = true;
                        }
                        if ($token->endIndex == 0) {
                            $token->continueToEnd = true;
                        }
                    }
                    break;
                    //Extend Here: Add other types
                }
                break;
            }
        }
        return $token;
    }

    public function getTokens() {
        return $this->tokens;
    }
}

