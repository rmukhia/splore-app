<?php

namespace App\Helpers\TokenReplacer;

use App\Helpers\TokenReplacer\Contracts\TokenReplacerContract;
use App\Category;

class TokenReplacer implements TokenReplacerContract
{
    private $lexers = array();

    public function blastOff()
    {

        return 'Houston, we have ignition';

    }

    public function __construct(){
        // do something
    }

    public function render($categoryName, $subject, $data) {
        if (!isset($this->lexers[$categoryName])) {
            $lexer = new Lexer($subject);
            $lexer->compile();
            $this->lexers[$categoryName] = $lexer;
        }

        $lexer = $this->lexers[$categoryName];
        
        $result = Renderer::render($lexer->getTokens(), $data);
        return $result;
    }

}
