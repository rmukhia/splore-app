<?php
namespace App\Helpers\TokenReplacer;

abstract class Renderer {
    const NOMATCHINGFIELD = "<Exception: Matching field not found.>";
    const MULTIFIELDNOTALLOWED = "<Exception: Muliple Field not allowed on single value token.>";
    const SINGLEFIELDNOTALLOWED = "<Exception: Single Field not allowd on multi value token.>";  

    private static function processTokenFiller($token, $data){
        return $token->value;
    }

    private static function processTokenSingle($token, $data) {
        $result = '';
        if (!isset($data[$token->id])) {
            return self::NOMATCHINGFIELD;
        }
        
        $result = is_array($data[$token->id])? 
            self::MULTIFIELDNOTALLOWED : $data[$token->id];
        return $result;
    }

    private static function processTokenMultiple($token, $data) {
        $result = '';
        if (!isset($data[$token->id])) {
            return self::NOMATCHINGFIELD;
        }
        $value = $data[$token->id];

        if (!is_array($value)) {
            return self::SINGLEFIELDNOTALLOWED;
        }


        $totalLen = count($value);
        
        $startIndex = $token->continueFromStart ? 
            0 : $token->startIndex;
        
        $len = 1;
        if ($token->continueToEnd) {
            $len = $totalLen - $startIndex;
        }
        else {
            $len = $token->endIndex == 0 
                ? 1 
                : $token->endIndex + 1 - $startIndex;
        }

        $values = array_slice($value, $startIndex, $len);

        foreach($values as $value) {
            $pvalue = gettype($value) == 'string'? 
                '"' . $value . '"':
                $value;
            $result = $result . $pvalue . ', ';
        }

        return substr($result,0,-2);
    }

    public static function render($tokens, $data) {
        $result = '';
        foreach($tokens as $token) {
            switch ($token->type) {
              case TokenType::FILLER:
                  $result = $result . self::processTokenFiller($token, $data);
                  break;
              case TokenType::SINGLE:
                  $result = $result . self::processTokenSingle($token, $data);
                  break;
              case TokenType::MULTIPLE:
                  $result = $result . self::processTokenMultiple($token, $data);
                  break;
            }
        }
        return $result;
    }
}
