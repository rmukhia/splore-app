<?php
namespace App\Helpers\TokenReplacer;

class Token {
    public $id; // the variable name
    public $type;
    public $value;
    // Multiple Value extra properties
    // startIndex the starting index 
    // endIndex the ending index 
    // continueFromStart start index from 0
    // continueToEnd end the index at the length of the array 

}
