<?php
namespace App\Helpers\TokenReplacer;

abstract class TokenType {	
    const FILLER = 1;
    const SINGLE = 2;
    const MULTIPLE = 3;
}
