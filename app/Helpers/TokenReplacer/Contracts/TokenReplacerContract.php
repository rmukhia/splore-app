<?php

namespace App\Helpers\TokenReplacer\Contracts;

Interface TokenReplacerContract
{
    public function blastOff();
}
