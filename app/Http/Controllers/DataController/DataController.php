<?php

namespace App\Http\Controllers\DataController;
use Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Post;
use App\PostElement;
use App\Http\Requests\DataSpreadsheetRequest;
use Illuminate\Support\Facades\Request as StaticRequest;

class DataController extends Controller
{
    /*
     * Different data types are
     * 	string
     * 	number
     * 	date
     */
    private function getContent($type, $element) {
        return $element[DataListImportHandler::$typeDict[$type]];
    }

    private function metaFromName($name) {
        $category = Category::where('name', '=', $name)->take(1)->get();
        return $category[0];
    }

    private function getData($posts, $schema) {
        $data = [];
        foreach ($posts as $post) {
            $dataElement  = [
                'id' => $post->id,
                'name' => $post->name,
                ];
            $schemaObj = json_decode($schema, true);
            $typeSchema = $schemaObj[DataListImportHandler::$schemaTypeKey];
            $multipleElementSchema = $schemaObj[DataListImportHandler::$schemaMultipleElementKey];

            foreach ($typeSchema as $key => $type) {
                $elements = PostElement::where('name', '=' , $key)->where('post_fk', '=' ,$post->id)->get();
                if ($multipleElementSchema[$key]) {
                    $dataElement[$key] = [];
                }
                foreach($elements as $element) {
                    if ($multipleElementSchema[$key]) {
                        array_push($dataElement[$key], $this->getContent($type, $element));
                    }
                    else {
                        $dataElement[$key] = $this->getContent($type, $element);
                    }
                }
            }
            array_push($data, $dataElement);
        }
        return $data;
    }

    /*
     * GET /api/index/{endpoint}/{pageId}?num={postPerPage}
     * 	If postPerPage is not defined, default value is 10.
     */
    protected function index(Request $request, $endpoint, $pageId) {
        $numPerPage = 10;
        if (isset($request['num'])) {
            $numPerPage = $request->num;
        }
        $category = $this->metaFromName($endpoint);
        $posts = Post::where('category_fk', '=', $category->id)->skip($pageId * $numPerPage)->take($numPerPage)->get();
        return $this->getData($posts, $category->schema);
    }

    /*
     * GET /api/{endpoint}/{id}
     */
    protected function view($endpoint, $id) {
        $schema = $this->metaFromName($endpoint)->schema;
        $post = Post::find($id);
        return $this->getData([$post], $schema)[0];
    }


    /**
     * GET /api/meta/{id}
     */
    protected function singleMeta($id) {
        $category = Category::find($id);
        return $category;
    }

    /**
     * GET /api/meta/name/{name}
     */
    protected function singleMetaFromName($name) {
        return $this->metaFromName($name);
    }

    /*
     * GET /api/index/meta
     */
    protected function meta() {
        $category = Category::all();
        return $category;
    }

    /*
     * GET /admin/category/data/spreadsheet
     */
    protected function spreadsheetUpload() {
        return view('admin.data-spreadsheet');
    }

    /*
     * POST admin/category/data/spreadsheet
     */
    protected function spreadsheetProcessor(DataSpreadsheetRequest $request) {
        if (StaticRequest::hasFile('spreadsheet-file'))
        {
            $file = StaticRequest::file('spreadsheet-file');
            StaticRequest::file('spreadsheet-file')->move(storage_path('exports'), 'data.xlsx');
            return redirect('/admin/spreadsheet/import');
        }

        return '{ "status" : "failed to upload file!"}';
    }

    protected function spreadsheetImport(Request $request, DataListImport $reader) {
        $reader->ignoreEmpty();
        $stats = $reader->handleImport();

        return view('admin.spreadsheet-report', [
            'stats' => $stats
            ]);
    }

    private function spreadsheetExportProcess($excel, $category, $dump) {
        $excel->sheet($category->name, function($sheet) use ($category, $dump) {
            $schema = json_decode($category->schema);
            $data = array();
            if ($dump) {
                //TODO: Dump all post here
            }
            else {
                $post = array();
                foreach($schema->type as $key => $value) {
                    $post[$key] = '';
                }
                array_push($data, $post);
            }

            $sheet->with($data);

        });
    }

    protected function spreadsheetTemplate(Category $category) {
        \Excel::create($category->name, 
            function($excel) use ($category) {
                $this->spreadsheetExportProcess($excel, $category, false);
            }
        )->export('xlsx');
    }

}
