<?php

namespace App\Http\Controllers\DataController;

class DataListImport extends \Maatwebsite\Excel\Files\ExcelFile {

    public function getFile() {
        return storage_path('exports') . '/data.xlsx';
    }

    public function getFilters()
    {
      return [
        'chunk'
      ];
    }
}
?>
