<?php
namespace App\Http\Controllers\DataController;

use App\Category;
use App\Post;
use App\PostElement;

use Auth;

use Log;

//TODO: stats

function debugLog($log) {
    if (true) {
        $log['file'] = 'DataListImportHandler';
        Log::debug('#' . json_encode($log) . ',');
    }
}

/*
 * There are three rules
 * - Never update post name ,skips
 * - If Single Data Element, then delete the previous Element and add New. If the element is already present do nothing.
 * - If Multi Data Element, if there is already duplicate element present no not append.
 */ 
class DataListImportHandler implements \Maatwebsite\Excel\Files\ImportHandler {
    // Name of 'type' json from schema
    public static $schemaTypeKey = 'type';
    // Multiple identifier for PostElement
    public static $schemaMultipleElementKey = 'multiple';
    // Post meta data schema
    public static $metaSchema = [
        'name' => 'string'
        ];
    // Key identifier from Post
    public static $postMetaKey = 'name';

    public static $typeDict = [
        'string' => 'text_content',
        'number' => 'numeric_content',
        'date' => 'date_content'
        ];

    public function handle($reader)
    {
        $stats = [];
        $results = $reader->get();
        foreach ($results as $sheet) {
            $categoryName = $sheet->getTitle();

            list($schema, $categoryId) =
                $this->getSchemaAndCategory($categoryName);
            if (!$schema) {
                // Pushed into ignored categories. Continue without.
                continue;
            }


            foreach ($sheet as $row) {
                if (isset($row[self::$postMetaKey])) {
                    // A row should have a unique key.
                    $log = [
                        'title' => 'Call -> processRow',
                        'function' => 'handle',
                        'row' => $row,
                        'schema' => $schema,
                        'categoryId' => $categoryId
                        ];
                    debugLog($log);

                    $name = $row[self::$postMetaKey];
                    if (!isset($stats[$name])) {
                        $stats[$name] = [];
                    }
                    $rowStats = $this->processRow($row, $schema, $categoryId);

                    array_push($stats[$name], $rowStats);

                }
            }
        }

        return $stats;
    }

    private function getSchemaAndCategory($name) {
        $category = Category::where('name', '=', $name)->take(1)->get();
        if (count($category) > 0 ){
            $category = $category[0];
            return array(json_decode($category->schema, true), $category->id);
        }
        return array(null, null);
    }

    private function processRow($row, $schema, $categoryId) {
        $stats = [];
        $metaElements = [];
        $postElements = [];

        /*
         * Building $metaElements and $postElements.
         */
        foreach ($row as $key => $value) {
            $key = trim($key);
            $value = trim($value);
            if (array_key_exists($key, self::$metaSchema)) {
                $metaElements[$key] = $value;
            }
            else if(array_key_exists($key, $schema[self::$schemaTypeKey])) {
                $postElements[$key] = $value;
            }
            else {
                // discard
            }
        }

        /*
         * check if Post requested for creation has a name in the request.
         * if it has the request, process the Post, otherwise discard.
         */

        $name = '';

        if (isset($metaElements[self::$postMetaKey])) {
            $name = $metaElements[self::$postMetaKey];

            $log = [
                'title' => 'Call -> processPost',
                'function' => 'processRow',
                'metaElements' => $metaElements,
                'postElements' => $postElements,
                'categoryId' => $categoryId,
                'schema' => $schema,
                'name' => $name
                ];
            debugLog($log);

            $postStats = $this->processPost($metaElements, $postElements, $categoryId,
                $schema, $name);

            // Account stats
            $stats[$name] = $postStats;
        }
        else {
            //TODO: Handle posts without name
        }

        return $stats;
    }

    private function processPost($metaElements, $postElements, $categoryId,
        $schema, $name) {
            $stats = [];
            $post = Post::where(self::$postMetaKey,'=',$name)->take(1)->get();

            if (count($post) > 0) {
                // There is already a Post with this name
                $post = $post[0];
                $log = [
                    'title' => 'Retrive Post',
                    'function' => 'processPost',
                    'name' => $name,
                    'post' => $post
                    ];
                debugLog($log);
            }
            else {
                // create a new Post
                $post = new Post;
                $post->name = $name;
                $post->category_fk = $categoryId;
                //TODO: Should Auth::check() be called somewhere up the down the call stack?
                $post->user_fk = Auth::user()->id;
                $post->save();
                $log = [
                    'title' => 'Create Post',
                    'function' => 'processPost',
                    'name' => $name,
                    'post' => $post
                    ];
                debugLog($log);

            }

            $log = [
                'title' => 'Call -> processMeta',
                'function' => 'processPost',
                'post' => $post,
                'metaElements' => $metaElements,
                'categoryId' => $categoryId
                ];
            debugLog($log);
            $metaStats = $this->processMeta($post, $metaElements, $categoryId);

            $log = [
                'title' => 'Call -> processElements',
                'function' => 'processPost',
                'post' => $post,
                'schema' => $schema,
                'postElements' => $postElements
                ];
            debugLog($log);
            $postStats = $this->processElements($post, $schema, $postElements);

            // Account Stats
            $stats['meta'] = $metaStats;
            $stats['elements'] = $postStats;

            return $stats;
        }

    private function processMeta($post, $metaElements, $categoryId) {
        $stats = [];
        foreach ($metaElements as $key => $value) {
            $metaStats = [
                'key' => $key,
                'value' => $value
                ];

            if ($key == self::$postMetaKey) {
                // skip unique post identifier
                continue;
            }
            $post[$key] = $value;

            // Account stats
            $stats[$key] = $value;
        }
        // Save the post again.
        $post->save();

        return $stats;
    }

    private function processElements($post, $schema, $postElements) {
        $stats = [];
        foreach ($postElements as $name => $value) {
            if ($value == "" || $value == null) {
                // Skip if no value
                continue;
            }
            $type = $schema[self::$schemaTypeKey][$name];
            $contentKey = self::$typeDict[$type];

            // Check if there are PostElement with the same
            // * post_fk
            // * name
            // * content

            $postElement = PostElement::where('post_fk', '=', $post->id)->where('name', '=', $name);

            // This checks if the element is already present
            if (PostElement::where($contentKey, '=', $value)->count() > 0) {
                continue;
            }

            // Delete the previous element to add new one for single valued field.
            if (!$schema[self::$schemaMultipleElementKey][$name]) {
                $postElement = $postElement->get();

                foreach($postElement as $element) {
                    $element->delete();
                }
            }

            // Add this new PostElement
            $newPostElement = new PostElement;
            $newPostElement->post_fk = $post->id;
            $newPostElement->name = $name;
            $newPostElement->type = $type;
            $newPostElement[$contentKey] = $value;
            $newPostElement->save();

            //Account stats
            $stats[$name] = [ 'type' => $type, 'value' => $value ];

        }

        return $stats;
    }
}
