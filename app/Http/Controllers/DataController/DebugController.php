<?php
namespace App\Http\Controllers\DataController;
use Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Post;
use App\PostElement;
use App\Http\Requests\DataSpreadsheetRequest;
use Auth;

use App\Helpers\TokenReplacer\Contracts\TokenReplacerContract;

class DebugController extends Controller {
    protected function index() {
        return view('admin.debug');
    }

    protected function deletePost() {
        $userId = Auth::user()->id;
        // Delete all postElements
        $postElements = PostElement::all();
        foreach($postElements as $postElement) {
            $postElement->delete();
            Log::debug($postElement);
        }
        // Delete all posts
        $posts = Post::all();
        foreach($posts as $post) {
            Log::debug($post);
            $post->delete();
        }
        return redirect('/admin/debug');
    }

    protected function runSomething(TokenReplacerContract $tokenReplacer){
        $result = $tokenReplacer->render('articles',
            '{"name":"#name#", "phonenumber": [#number[0:-2]#]}', 
            [ 'name' => 'Raunak', 'number' => [1,2,3,4,5]]);
        return response()->json(json_decode($result));
    }

}
