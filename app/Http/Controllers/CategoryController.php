<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\EditCategoryRequest;

use App\Category;
use App\Post;

use Log;

class CategoryController extends Controller
{
	/*
	* GET category
	* List all categories
	*/
	protected function index() {
		$result = [];
		$categories = Category::orderBy('created_at', 'asc')->get();

		foreach($categories as $category) {
			$category->numPost = Post::where('category_fk', '=', $category->id)->count();
		}

		return view('admin.category-index', [
			'categories' => $categories
		]);
	}

	/*
	* GET category/create
	* Form to create new category
	*/
	protected function create() {
		return view('admin.category-create');
	}

	/*
	* POST category
	* Save a category
	*/
	protected function store(CreateCategoryRequest $request) {
		$category = new Category;
		$category->name = $request->input('name');
		$category->schema = '{}';
		$category->save();
        $editUrl = '/admin/category/' . $category->id . '/edit';
        return redirect($editUrl);
	}

	/*
	* GET category/{id}
	* List category with id
	*/
	protected function show(Category $category) {
		return $category->toJson();
	}

	/*
	* GET category/{id}/edit
	* Form to edit category with id
	*/
	protected function edit(Category $category) {
		return view('admin.category-edit', [
			'category' => $category
		]);
	}

	/*
	* PATCH/PUT category/{id}
	* Update a category with id
	*/
	protected function update(EditCategoryRequest $request, Category $category) {
		$category->name = $request->input('name');
		$category->schema = $request->input('schema');
		$category->save();
		return $category->toJson();
	}

	/*
	* DELETE category/{id}
	* Delete a category with id
	*/
	protected function destroy(Category $category) {
		$category->delete();
		return $category->toJson();
	}
}
