<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\User; 

class UserController extends Controller
{
    protected function index() {
        $users = User::orderBy('created_at', 'asc')->get();
        return view('admin.user-index', [
            'users' => $users
        ]);
    } 

    protected function destroy(User $user){
        $currentUser = Auth::user();
        if ($user != $currentUser) {
            $user->delete();
        }
        return $user;
    }

    protected function promote(User $user){
        $user->admin = true;
        $user->save();
    }

    protected function demote(User $user){
        $user->admin = false;
        $user->save();
    }
}
