<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use App\PostElement;

use App\Http\Controllers\DataController\DataListImportHandler;

use App\Http\Requests\CreatePostRequest;

use Auth;
use Log;

class PostController extends Controller
{
    /*
     * GET admin/post?category={category_id}
     * List all belonging to category_id
     */
    protected function index(Request $request) {
        $categoryId= $request['category'];
        $posts = Post::where('category_fk', '=' , $categoryId)->get();
        return view('admin.post-index',[
            'categoryId' => $categoryId,
            'posts' => $posts
            ]);
    }

    protected function store(CreatePostRequest $request) {
        $name = $request['name'];
        $category = $request['category'];
        $post = new Post;
        $post->name = $name;
        $post->category_fk = $category;
        $post->user_fk = Auth::user()->id;
        $post->save();
        return $post;
    }

    protected function destroy(Post $post) {
        foreach($post->postElements as $postelement) {
            $postelement->delete();
        }
        $post->delete();
        return $post;
    }

    protected function create() {
        return view('admin.post-create');
    }

    protected function edit(Post $post) {
        $processedPostElements = array();
        $addFields = array();
        $htmlInputs = [
            'string' => 'text',
            'number' => 'number',
            'date' => 'date'
            ];

        $postElements = $post->postElements;
        $schema = json_decode($post->category->schema, true);
        $schemaType = $schema['type'];

        foreach($schemaType as $key => $value) {
            // To show the form as it is entered in the schema
            $found = false;
            foreach ($postElements as $postElement) {
                if ($postElement->name == $key && !isset($postElement['dirty'])) {
                    $result = [];
                    $result['id'] = $postElement->id;
                    $result['name'] = $postElement->name;
                    $result['type'] = $postElement->type;
                    $result['htmlType'] = $htmlInputs[$result['type']];
                    $contentKey = DataListImportHandler::$typeDict[$result['type']];
                    $result['content'] = $postElement[$contentKey];
                    array_push($processedPostElements, $result);
                    $postElement['dirty'] = true;
                    $found = true;
                }
            }


            $multiple = $schema['multiple'][$key];
            if ($multiple || !$found) {
                $result = [];
                $result['name'] = $key;
                $result['type'] = $value;
                $result['multiple'] = $multiple;
                $result['htmlType'] = $htmlInputs[$result['type']];
                array_push($addFields, $result);
            }
        }

        return view('admin.post-edit',[
            'post' => $post,
            'postElements' => $processedPostElements,
            'addFields' => $addFields
            ]);
    }

    /*
     * POST admin/post/{post}/append
     * Adds a new PostElement to a post.
     */
    protected function appendPostElements(Request $request, Post $post) {
        $schema = json_decode($post->category->schema, true);
        $schemaType = $schema['type'];
        $input = $request->all();
        foreach ($input as $key => $value) {
            if (!array_key_exists($key, $schemaType) || $value == "") {
                continue;
            }
            $type = $schemaType[$key];
            $postElement = new PostElement;
            $postElement->post_fk = $post->id;
            $postElement->name = $key;
            $postElement->type = $type;
            $postElement[DataListImportHandler::$typeDict[$type]] = $value;
            $postElement->save();
        }
        return $post;
    }

    protected function updatePostElements(Request $request, Post $post) {
        $input = $request->all();
        foreach ($input as $key => $value) {
            $postElement = PostElement::find($key);
            if (!$postElement) {
                continue;
            }
            $type = $postElement->type;
            $postElement[DataListImportHandler::$typeDict[$type]] = $value;
            $postElement->save();
        }
        return $post;
    }

    protected function deletePostElements(Request $request, Post $post) {
        $input = $request->all();
        foreach ($input as $key => $value) {
            $postElement = PostElement::find($key);
            if (!$postElement) {
                continue;
            }
            $postElement->delete();
        }
        return $post;
    }
}
