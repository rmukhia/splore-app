<?php

use App\Category;
use App\User;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

/*
 *  Admin routes
 *  Only admin can access these routes.
 */
Route::group(['middleware' => ['auth','admin']], function () {
    // The main admin index place
    Route::get('admin', function(){
        $categories = Category::all();
        $users = (object) [
            'count' => User::all()->count(),
            'adminCount' => User::where('admin', '=' , true)->count(),
            'regularCount' => User::where('admin', '=' , false)->count()
            ];

        return view('admin.index', [
            'categories' => $categories,
            'users' => $users
        ]);
    });

    // Category
    Route::resource('admin/category', 'CategoryController');

    Route::get('admin/user', 'UserController@index');
    Route::delete('admin/user/{user}', 'UserController@destroy');
    Route::put('admin/user/{user}/promote', 'UserController@promote');
    Route::put('admin/user/{user}/demote', 'UserController@demote');

    // Post
    // BUG: Mapping to user not done yet.
    Route::get('admin/post', 'PostController@index');
    Route::post('admin/post', 'PostController@store');
    Route::delete('admin/post/{post}', 'PostController@destroy');
    Route::get('admin/post/{post}/edit', 'PostController@edit');
    Route::get('admin/post/create', 'PostController@create');

    Route::post('admin/post/{post}/append-elements', 'PostController@appendPostElements');
    Route::put('admin/post/{post}/update-elements', 'PostController@updatePostElements');
    Route::delete('admin/post/{post}/delete-elements', 'PostController@deletePostElements');

    // Spreadsheet
    // BUG: Upload is not working.
    Route::get('admin/spreadsheet', 'DataController\DataController@spreadsheetUpload');
    Route::post('admin/spreadsheet', 'DataController\DataController@spreadsheetProcessor');
    Route::get('admin/spreadsheet/import', 'DataController\DataController@spreadsheetImport');
    Route::get('admin/spreadsheet/{category}', 'DataController\DataController@spreadsheetTemplate');

    // Debug
    Route::get('admin/debug', 'DataController\DebugController@index');
    Route::delete('admin/debug/post', 'DataController\DebugController@deletePost');
    Route::get('admin/debug/run', 'DataController\DebugController@runSomething');
});



// API to access data
Route::get('api/index/meta', 'DataController\DataController@meta');
Route::get('api/meta/{id}', 'DataController\DataController@singleMeta');
Route::get('api/meta/name/{name}', 'DataController\DataController@singleMetaFromName');

Route::get('api/index/{endpoint}/{page}', 'DataController\DataController@index');
Route::get('api/{endpoint}/{id}', 'DataController\DataController@view');
