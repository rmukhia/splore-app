# xlsx to API

A WebApp that converts xlsx files to APIs that can retrieve data.

## How to Use
Create a spreadsheet.
Enter the following and name the sheet as `Shop`

| name           | title              | phonenumber |
|----------------|--------------------|-------------|
| brightfruit    | Bright Fruit Store | 0000000001  |
| brightfruit    |                    | 0000000002  |
| darkshoe       | Dark Shoe Store    | 1000000000  |
| darkshoe       |                    | 2000000000  |
| mellowcloth    | Mellow Cloth Store | 0000010000  |

Create another sheet and name the sheet as `Service`. Enter the following data to the sheet.

| name           | title              | phonenumber | popularity  |
|----------------|--------------------|-------------|-------------|
| brightlaundry  | Bright Laundry     | 0000000001  | Not Popular |
| brightlaundry  |                    | 0000000002  |             |
| darkpub        | Dark Pub           | 1000000000  | Popular     |
| darkpub        |                    | 2000000000  |             |
| mellowbarber   | Mellow Barber      | 0000010000  | Very Popular|

In the `admin` panel of this webapp, create two Categories.

Name one category as `Shop` with the following schema.
```json
{
  "type" : {
    "title" :  "string",
    "phonenumber" : "number"
  },
 "multiple": {
   "title" :  false,
   "phonenumber" : true
 }
}
```
<br>
Name another category as `Service` with the following schema.
```json
{
  "type" : {
    "title" :  "string",
    "phonenumber" : "number",
    "popularity" : "string"
  },
 "multiple": {
   "title" :  false,
   "phonenumber" : true,
   "popularity" : false
 }
}
```
<br>
Upload the spreadsheet in this webapp.
The uploaded spreadsheet will be transformed and stored in the DB that this webapp uses.

After the upload you will have access to the following web json APIs.

Type the following urls in your browser prefixed with the hostname and port where you are hosting this webapp.
For example if you are serving this webapp from `http://yourhost:port`, then open `http://yourhost:port/api/index/Shop/0?num=2`

Lets try out the following APIs.

`/api/index/Shop/0?num=2`

Reply will be
```json
[{"id" : 1, "name": "brightfruit", "title" : "Bright Fruit Store", "phonenumber" : ["0000000001", "0000000002"]},{"id" : 2, "name": "darkshoe", "title" : "Dark Shoe Store", "phonenumber" : ["1000000000", "2000000000"]}]
```
<br>
`/api/index/Shop/1?num=2`

Reply will be
```json
[{"id" : 3, "name": "mellowcloth", "title" : "Mellow Cloth Store", "phonenumber" : ["0000010000"]}]
```
<br>
`/api/index/Service/0`

Reply will be
```json
[
{"id" : 4, "name": "brightlaundry", "title" : "Bright Laundry", "phonenumber" : ["0000000001", "0000000002"], "popularity" : "Not Popular"},
{"id" : 5, "name": "darkpub", "title" : "Dark Shoe Store", "phonenumber" : ["1000000000", "2000000000"], "popularity": "Popular"},
{"id" : 6, "name": "mellowbarber", "title" : "Mellow Barber", "phonenumber" : ["0000010000"], "popularity": "Very Popular"}
]
```


# APIs Available
#### Category API
* `/api/index/meta` : list all categories along with their schema.
* `/api/meta/{id}` : get category and schema with primary key `{id}`.
* `/api/meta/name/{name}` : get category and schema with name as `{name}`

#### Endpoint API
* `/api/index/{endpoint}/{page}[?num={num}]` : list entries with sheet name `{endpoint}`. `{page}` is the page number to list. `{num}` is the number of entries per page. If `[?num={num}]` is omitted, the default number of entries per page is 10.
* `/api/{endpoint}/{id}`:  get entry from sheet name `{endpoint}` with primary key `{id}`.

# Notes
Uploading a spreadsheet will append new entries to existing data.
* If an entry with  `name` already exists, then fields with multiple set to true will be appended to the entry.
* If an entry with  `name` already exists, then fields with multiple set to false will be updated in the entry.

# Backlogs
* Refactor
* Sync with spreadsheet on cloud service like google drive
* Exception Handling

This project is made with Laravel PHP Framework. Uses [Maatwebsite/Laravel-Excel](https://github.com/Maatwebsite/Laravel-Excel) for reading data from `xlsx`.

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
